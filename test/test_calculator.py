"""
Unit tests for the calculator library
"""


import sys

sys.path.insert(0, "/home/sharath/gitlab/simple_python_project/calculator")

from calculator.calculator import *


class TestCalculator:
    def test_addition(self):
        assert 4 == add(2, 2)

    def test_subtraction(self):
        assert 2 == subtract(4, 2)
